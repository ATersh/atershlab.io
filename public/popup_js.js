// show the popup
function popup_show(e) {
    var popup = document.getElementById('popup-container');
    var op = 0;
    popup.style.opacity = op;
    popup_action("#sendmessage", "block");
    function step() {
        op += 0.04;
        popup.style.opacity = op;
        if (op < 1) {
            requestAnimationFrame(step);
        }
    }
    requestAnimationFrame(step);
}

// hide the popup
function popup_hide(e) {
    var popup = document.getElementById('popup-container');
    var op = 1;

    function step() {
        if (op > 0) {
            op -= 0.04;
            popup.style.opacity = op;
            requestAnimationFrame(step);
        }
        else {
            popup.style.opacity = 0;
            popup_action("", "none");
        }
    }
    requestAnimationFrame(step);
}

function popup_action(uri, state) {
    // find elem and change style
    var popup = document.getElementById('popup-container');
    popup.style.display = state;

    // change uri
    history.pushState(null, null, 'index.html' + uri);
};

window.addEventListener ('popstate', function(e){
    popup_hide(e);
})